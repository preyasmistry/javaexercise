package com.brevitaz.pizzasimulation;

import com.brevitaz.pizzasimulation.entities.Chef;
import com.brevitaz.pizzasimulation.entities.Customer;
import com.brevitaz.pizzasimulation.entities.PizzeriaManager;
import com.brevitaz.pizzasimulation.entities.PizzeriaWaiter;
import com.brevitaz.pizzasimulation.entities.pizzaentities.Order;
import com.brevitaz.pizzasimulation.entities.pizzaentities.Pizza;
import com.brevitaz.pizzasimulation.entities.pizzaentities.Pizzeria;
import com.brevitaz.pizzasimulation.interfaces.Manager;

public class PizzaMakingSimulation {
    public static void main(String[] args) {

        Pizzeria pizzeria = new Pizzeria();
        Customer customer = new Customer();
        Manager manager = new PizzeriaManager();

        Order order = customer.placeOrder(customer.selectToppings(new String[]{"corn", "olive", "tomato", "onion", "mushroom"}),
                customer.selectCheese(new String[]{"ricotta", "provolone", "mozzarella", "cheddar"}),
                customer.selectSauce(new String[]{"salsa", "pesto", "marinara"}));

        Pizza pizza = manager.assignOrder(order, new Chef());
        customer.receiveFood(new PizzeriaWaiter().serve(pizza));
    }
}
