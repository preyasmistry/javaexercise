package com.brevitaz.pizzasimulation.entities;

import com.brevitaz.pizzasimulation.entities.pizzaentities.Order;
import com.brevitaz.pizzasimulation.entities.pizzaentities.Pizza;
import com.brevitaz.pizzasimulation.interfaces.Manager;

public class PizzeriaManager implements Manager {
    @Override
    public String toString() {
        return "Pizzeria Manager";
    }

    @Override
    public Pizza assignOrder(Order order, Chef chef) {
        return chef.takeOrder(order);
    }
}
