package com.brevitaz.pizzasimulation.entities;

import com.brevitaz.pizzasimulation.entities.pizzaentities.*;
import com.brevitaz.pizzasimulation.interfaces.Cheese;
import com.brevitaz.pizzasimulation.interfaces.Ingredient;
import com.brevitaz.pizzasimulation.interfaces.Sauce;
import com.brevitaz.pizzasimulation.interfaces.Topping;

import java.util.ArrayList;
import java.util.List;

public class Chef {

    @Override
    public String toString() {
        return "Chef";
    }

    private Oven oven = new Oven();
    private PizzaCutter pizzaCutter = new PizzaCutter();
    private Pizzeria pizzeria = new Pizzeria();
    private List<Ingredient> doughIngredients = new Pizzeria().getIngredientsList();

    public Pizza takeOrder(Order order) {
        return makePizza(order);
    }

    private List prepareToppings(String[] selectedToppings) {
        List<Topping> toppingList = new ArrayList<>();
        for (String topping : selectedToppings)
            if (pizzeria.getToppingMap().containsKey(topping)) toppingList.add(pizzeria.getToppingMap().get(topping));
            else System.out.println("Oops sorry '" + topping + "' is currently not available.");
        return toppingList;
    }

    private List prepareCheese(String[] selectedCheese) {
        List<Cheese> cheeseList = new ArrayList<>();
        for (String cheese : selectedCheese)
            if (pizzeria.getCheeseMap().containsKey(cheese)) cheeseList.add(pizzeria.getCheeseMap().get(cheese));
            else System.out.println("Oops sorry '" + cheese + "' is currently not available.");
        return cheeseList;
    }

    private List prepareSauces(String[] selectedSauces) {
        List<Sauce> sauceList = new ArrayList<>();
        for (String sauce : selectedSauces)
            if (pizzeria.getSauceMap().containsKey(sauce)) sauceList.add(pizzeria.getSauceMap().get(sauce));
            else System.out.println("Oops sorry '" + sauce + "' is currently not available.");
        return sauceList;
    }

    private Pizza makePizza(Order order) {
        System.out.println("Step-1 : Take the ingredients and make dough.");
        Dough dough = makeDough(doughIngredients);
        System.out.println("Step-2 : Roll the dough to make Base for Pizza.");
        Base base = roll(dough);
        System.out.println("Step-3 : Add toppings, add cheese.");
        addToppings(base, prepareToppings(order.getSelectedToppings()));
        addCheese(base, prepareCheese(order.getSelectedCheese()));
        System.out.println("Step-4 : Spread the sauce.");
        spread(base, prepareSauces(order.getSelectedSauce()));
        System.out.println("Step-5 : Bake the Pizza.");
        Pizza pizza = (Pizza) oven.bake(base, 20, 200);
        System.out.print("Step-6 : ");
        pizzaCutter.cut(pizza);
        return pizza;
    }

    private Dough makeDough(List<Ingredient> ingredients) {
        return mix(ingredients);
    }

    private Dough mix(List<Ingredient> ingredients) {
        return new Dough(ingredients);
    }

    private Base roll(Dough dough) {
        return new Base(dough);
    }

    private void spread(Base base, List<Sauce> sauce) {
        base.setSauce(sauce);
    }

    private void addToppings(Base base, List<Topping> toppings) {
        base.setToppings(toppings);
    }

    private void addCheese(Base base, List<Cheese> cheese) {
        base.setCheese(cheese);
    }
}
