package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Sauce;

public class Salsa implements Sauce {
    @Override
    public String toString() {
        return "Salsa sauce";
    }
}
