package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Sauce;

public class Pesto implements Sauce {
    @Override
    public String toString() {
        return "Pesto sauce";
    }
}
