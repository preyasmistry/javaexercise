package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Topping;

public class Tomato implements Topping {
    @Override
    public String toString() {
        return "Tomato";
    }
}
