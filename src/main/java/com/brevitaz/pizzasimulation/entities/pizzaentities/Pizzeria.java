package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Cheese;
import com.brevitaz.pizzasimulation.interfaces.Ingredient;
import com.brevitaz.pizzasimulation.interfaces.Sauce;
import com.brevitaz.pizzasimulation.interfaces.Topping;

import java.util.*;

public class Pizzeria {

    private List<Ingredient> ingredientsList = new ArrayList<>();
    private Map<String, Topping> toppingMap = new HashMap<>();
    private Map<String, Sauce> sauceMap = new HashMap<>();
    private Map<String, Cheese> cheeseMap = new HashMap<>();

    public Pizzeria() {
        ingredientsList.add(new Flour());
        ingredientsList.add(new Yeast());
        ingredientsList.add(new Sugar());
        ingredientsList.add(new Water());

        sauceMap.put("salsa", new Salsa());
        sauceMap.put("pesto", new Pesto());
        sauceMap.put("marinara", new Marinara());

        toppingMap.put("olive", new Olive());
        toppingMap.put("tomato", new Tomato());
        toppingMap.put("onion", new Onion());
        toppingMap.put("mushroom", new Mushroom());

        cheeseMap.put("ricotta", new Ricotta());
        cheeseMap.put("provolone", new Provolone());
        cheeseMap.put("mozzarella", new Mozzarella());
        cheeseMap.put("cheddar", new Cheddar());
    }

    public List<Ingredient> getIngredientsList() {
        return ingredientsList;
    }

    public Map<String, Topping> getToppingMap() {
        return toppingMap;
    }

    public Map<String, Sauce> getSauceMap() {
        return sauceMap;
    }

    public Map<String, Cheese> getCheeseMap() {
        return cheeseMap;
    }
}
