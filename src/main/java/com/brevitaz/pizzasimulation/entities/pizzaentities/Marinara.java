package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Sauce;

public class Marinara implements Sauce {
    @Override
    public String toString() {
        return "Marinara sauce";
    }
}
