package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Cheese;

public class Mozzarella implements Cheese {
    @Override
    public String toString() {
        return "Mozzarella Cheese";
    }
}
