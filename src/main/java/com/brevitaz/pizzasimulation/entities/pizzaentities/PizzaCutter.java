package com.brevitaz.pizzasimulation.entities.pizzaentities;

public class PizzaCutter {
    @Override
    public String toString() {
        return "Pizza Cutter";
    }

    public void cut(Pizza pizza) {
        System.out.print("Cut " + pizza + " in slices.\n");
    }
}
