package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Ingredient;

public class Water implements Ingredient {
    @Override
    public String toString() {
        return "Water";
    }
}
