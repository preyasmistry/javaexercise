package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Ingredient;

public class Sugar implements Ingredient {
    @Override
    public String toString() {
        return "Sugar";
    }
}
