package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.*;

import java.util.List;

public class Base implements Bakeable {

    private Dough dough;

    private List<Sauce> sauce;

    private List<Cheese> cheese;

    private List<Topping> toppings;

    public Base(Dough dough) {
        this.dough = dough;
    }

    public Dough getDough() {
        return dough;
    }

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public List<Sauce> getSauce() {
        return sauce;
    }

    public void setSauce(List<Sauce> sauce) {
        this.sauce = sauce;
    }

    public List<Cheese> getCheese() {
        return cheese;
    }

    public void setCheese(List<Cheese> cheese) {
        this.cheese = cheese;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    @Override
    public String toString() {
        return "Base";
    }

    @Override
    public Pizza bake() {
        return new Pizza(this);
    }
}
