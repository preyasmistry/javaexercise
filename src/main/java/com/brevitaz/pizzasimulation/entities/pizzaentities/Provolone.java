package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Cheese;

public class Provolone implements Cheese {
    @Override
    public String toString() {
        return "Provolone Cheese";
    }
}
