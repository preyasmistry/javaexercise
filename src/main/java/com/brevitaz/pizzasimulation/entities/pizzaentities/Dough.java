package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Food;
import com.brevitaz.pizzasimulation.interfaces.Ingredient;

import java.util.List;

public class Dough implements Food {

    private List<Ingredient> ingredients;

    public Dough(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    @Override
    public String toString() {
        return "Dough is made of " +
                ingredients;
    }
}
