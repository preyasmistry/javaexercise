package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Cheese;

public class Ricotta implements Cheese {
    @Override
    public String toString() {
        return "Ricotta Cheese";
    }
}
