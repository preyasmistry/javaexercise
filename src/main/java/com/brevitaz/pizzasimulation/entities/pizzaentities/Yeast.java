package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Ingredient;

public class Yeast implements Ingredient {
    @Override
    public String toString() {
        return "Yeast";
    }
}
