package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Food;

public class Pizza extends BakedFood implements Food {

    private Base base;

    public Pizza(Base base) {
        this.base = base;
    }

    @Override
    public String toString() {
        return "Pizza";
    }

    public Base getBase() {
        return base;
    }

}
