package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Cheese;

public class Cheddar implements Cheese {
    @Override
    public String toString() {
        return "Cheddar Cheese";
    }
}
