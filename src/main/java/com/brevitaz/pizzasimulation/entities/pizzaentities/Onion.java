package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Topping;

public class Onion implements Topping {
    @Override
    public String toString() {
        return "Onion";
    }
}
