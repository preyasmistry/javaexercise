package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Topping;

public class Olive implements Topping {
    @Override
    public String toString() {
        return "Olive";
    }
}
