package com.brevitaz.pizzasimulation.entities.pizzaentities;

public class Order {

    private String[] selectedToppings;
    private String[] selectedCheese;
    private String[] selectedSauce;

    public Order(String[] selectedToppings, String[] selectedCheese, String[] selectedSauce) {
        this.selectedToppings = selectedToppings;
        this.selectedCheese = selectedCheese;
        this.selectedSauce = selectedSauce;
    }

    public String[] getSelectedToppings() {
        return selectedToppings;
    }

    public String[] getSelectedCheese() {
        return selectedCheese;
    }

    public String[] getSelectedSauce() {
        return selectedSauce;
    }
}
