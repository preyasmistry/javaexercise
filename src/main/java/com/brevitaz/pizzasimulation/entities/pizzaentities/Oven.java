package com.brevitaz.pizzasimulation.entities.pizzaentities;

import com.brevitaz.pizzasimulation.interfaces.Bakeable;

public class Oven {
    public BakedFood bake(Bakeable bakeable, int timeInMinutes, float temperatureInCelsius) {
        System.out.println(bakeable + " started baking at " + temperatureInCelsius + "° C for " + timeInMinutes + " minutes.");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(bakeable.bake() + " is Ready.");
        return bakeable.bake();
    }
}
