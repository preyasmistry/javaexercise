package com.brevitaz.pizzasimulation.entities;

import com.brevitaz.pizzasimulation.entities.pizzaentities.Order;
import com.brevitaz.pizzasimulation.interfaces.Food;

public class Customer {
    @Override
    public String toString() {
        return "Customer";
    }

    public String[] selectSauce(String[] sauceList) {
        return sauceList;
    }

    public String[] selectToppings(String[] toppingsList) {
        return toppingsList;
    }

    public String[] selectCheese(String[] cheeseList) {
        return cheeseList;
    }

    public Order placeOrder(String[] selectedToppings, String[] selectedCheese, String[] selectedSauce) {
        return new Order(selectedToppings, selectedCheese, selectedSauce);
    }

    public void receiveFood(Food food) {
        eat(food);
    }

    private void eat(Food food) {
        System.out.println("\n" + this + " is eating " + food + ".");
    }
}
