package com.brevitaz.pizzasimulation.entities;

import com.brevitaz.pizzasimulation.entities.pizzaentities.Pizza;
import com.brevitaz.pizzasimulation.interfaces.Waiter;

public class PizzeriaWaiter implements Waiter {
    @Override
    public String toString() {
        return "Pizzeria's Waiter";
    }

    public Pizza serve(Pizza pizza) {
        System.out.println("\n" + this + " is serving " + pizza + " with " + pizza.getBase().getToppings() + " Toppings, "
                + pizza.getBase().getSauce() + " and with " + pizza.getBase().getCheese() + ".");
        return pizza;
    }
}
