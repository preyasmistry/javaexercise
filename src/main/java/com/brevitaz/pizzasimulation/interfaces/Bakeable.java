package com.brevitaz.pizzasimulation.interfaces;

import com.brevitaz.pizzasimulation.entities.pizzaentities.BakedFood;

public interface Bakeable extends Food {
    BakedFood bake();
}
