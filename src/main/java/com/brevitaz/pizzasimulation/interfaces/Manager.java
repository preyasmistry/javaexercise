package com.brevitaz.pizzasimulation.interfaces;

import com.brevitaz.pizzasimulation.entities.Chef;
import com.brevitaz.pizzasimulation.entities.pizzaentities.Order;

public interface Manager {
    <T extends Food> T assignOrder(Order order, Chef chef);
}
