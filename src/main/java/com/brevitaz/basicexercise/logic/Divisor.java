package com.brevitaz.basicexercise.logic;

public class Divisor {
    public void divide(int number) {
        for (int i = 1; i <= number / 2; i++) {
            if (number % i == 0) {
                System.out.print(i + " ");
            }
        }
    }
}
