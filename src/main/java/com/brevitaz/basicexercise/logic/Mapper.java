package com.brevitaz.basicexercise.logic;

public class Mapper {

    public String mapNumberToWord(int number) {
        if ((number != 0) && ((number % 3) == 0 && (number % 5) == 0)) {
            return ("FizzBuzz");
        } else if ((number != 0) && (number % 3 == 0)) {
            return ("Fizz");
        } else if ((number != 0) && (number % 5 == 0)) {
            return ("Buzz");
        } else {
            return ("Number: " + number);
        }
    }
}
