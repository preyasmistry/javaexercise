package com.brevitaz.basicexercise.logic;

public class LogicMain {
    public static void main(String[] args) {

        System.out.println("Practical-1 : \n" +
                new Mapper().mapNumberToWord(3) + "\n" +
                new Mapper().mapNumberToWord(5) + "\n" +
                new Mapper().mapNumberToWord(15) + "\n" +
                new Mapper().mapNumberToWord(0)
        );

        System.out.println("\n\nPractical-2 : \n");
        new Builder().build(4);

        System.out.println("\n\nPractical-3 : \n");
        new Divisor().divide(10);
    }
}
