package com.brevitaz.basicexercise.oom.school;

public class SchoolMain {
    public static void main(String[] args) {
        /* Kapil is teaching Java to Sunil. */
        System.out.println(new Teacher("Kapil").teach(new ProgrammingLanguage("Java")) + " to " + new Student("Sunil").getName()+".");
    }
}
