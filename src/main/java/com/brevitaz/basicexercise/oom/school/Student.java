package com.brevitaz.basicexercise.oom.school;

public class Student extends Person {
    public Student(String name) {
        super(name);
    }

    public Student() {
        super();
    }

    @Override
    public String toString() {
        return "Students";
    }
}
