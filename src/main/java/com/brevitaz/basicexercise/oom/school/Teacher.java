package com.brevitaz.basicexercise.oom.school;

public class Teacher extends Person {

    public Teacher(String name) {
        super(name);
    }

    String teach(Language programmingLanguage) {
        return this.getName() + " teaches " + programmingLanguage.getName();
    }
}
