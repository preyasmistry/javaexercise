package com.brevitaz.basicexercise.oom.cafe;

public class CafeMain {
    public static void main(String[] args) {
        /* 2. Prakash is serving tea. (Should be possible to serve coffee as well) */
        System.out.println(new Helper("Prakash").serve(new Tea("Green Tea")));
    }
}
