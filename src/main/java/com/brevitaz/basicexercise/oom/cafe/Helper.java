package com.brevitaz.basicexercise.oom.cafe;

import com.brevitaz.basicexercise.oom.school.Person;

public class Helper extends Person {

    public Helper(String name) {
        super(name);
    }

    String serve(Beverage beverage) {
        return getName() + " is serving " + beverage.getName() + ".";
    }
}
