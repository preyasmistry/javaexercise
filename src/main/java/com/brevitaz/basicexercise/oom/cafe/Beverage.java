package com.brevitaz.basicexercise.oom.cafe;

public class Beverage {
    private String name;

    public Beverage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
