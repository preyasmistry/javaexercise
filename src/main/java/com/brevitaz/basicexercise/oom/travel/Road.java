package com.brevitaz.basicexercise.oom.travel;

public class Road {
    int lean;
    String condition;

    public Road(int lean, String condition) {
        this.lean = lean;
        this.condition = condition;
    }
}
