package com.brevitaz.basicexercise.oom.travel;

public class TravelMain {
    public static void main(String[] args) {
        /* Tarang is travelling to Baroda in Maruti Swift with number GJ1 KG 5620 via Express Highway */
        Traveller traveller = new Traveller("Tarang", "Baroda");
        Car car = new Car("Maruti Swift", "GJ1 KG 5620");
        System.out.println(traveller.getName() + " is travelling to " + traveller.getDestination() + " in " + car.getName() + " with number " + car.getNumberPlate() + " via " + new ExpressHighway(6, "Good") + ".");
    }
}
