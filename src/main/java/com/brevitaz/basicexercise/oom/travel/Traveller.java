package com.brevitaz.basicexercise.oom.travel;

import com.brevitaz.basicexercise.oom.school.Person;

public class Traveller extends Person {
    private String destination;

    public Traveller(String name, String destination) {
        super(name);
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }
}
