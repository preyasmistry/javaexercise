package com.brevitaz.basicexercise.oom.travel;

public class Car extends Vehicle {

    private String numberPlate;

    public Car(String name, String numberPlate) {
        super(name);
        this.numberPlate = numberPlate;
    }

    public String getNumberPlate() {
        return numberPlate;
    }
}
