package com.brevitaz.basicexercise.oom.travel;

public class ExpressHighway extends Highway {

    public ExpressHighway(int lean, String condition) {
        super(lean, condition);
    }

    @Override
    public String toString() {
        return "Express Highway";
    }
}
