package com.brevitaz.basicexercise.oom.store;

import com.brevitaz.basicexercise.oom.school.Person;
import com.brevitaz.basicexercise.oom.school.Student;

public class Salesman extends Person {
    public Salesman(String name) {
        super(name);
    }

    public String sell(Laptop laptop, Student student) {
        return (laptop.toString() + " sell " + laptop.getName() + " to " + student.toString() + " at " + ((laptop.getPrice() < 40000) ? "low" : "high") + " cost.");
    }
}
