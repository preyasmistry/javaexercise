package com.brevitaz.basicexercise.oom.store;

public class Laptop extends Electronics {

    public Laptop(String name, double price) {
        super(name, price);
    }

    @Override
    public String toString() {
        return "Laptop";
    }
}
