package com.brevitaz.basicexercise.oom.store;

import com.brevitaz.basicexercise.oom.school.Student;

public class StoreMain {
    public static void main(String[] args) {
        /* Bill sells laptops to students at low cost. */
        System.out.println(new Salesman("Bill").sell(new Laptop("laptops", 30000), new Student()));
    }
}
